﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Routing.Internal;
using ToDoApi.Data.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ToDoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITasksRepository _taskRepository;
        private readonly ITagsRepository _tagRepository;


        public TasksController(ITasksRepository taskRepository, ITagsRepository tagRepository)
        {
            _taskRepository = taskRepository;
            _tagRepository = tagRepository;
        }

        // GET: api/<TaskController>
        [HttpGet]
        public ActionResult Get(int? tagId)
        {
            IEnumerable<Data.Models.Task> tasks;
            if (tagId == null)
                tasks = _taskRepository.GetAll();
            else
            {
                var tag = _tagRepository.GetById((int)tagId);
                if (tag == null)
                    return NotFound();
                else
                    tasks = _taskRepository.GetByTag(tag);
            }
            return new JsonResult(tasks);
        }

        // GET api/<TaskController>/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var task = _taskRepository.GetById(id);
            if (task == null)
                return NotFound();
            return new JsonResult(task);
        }

        /*// GET api/<TaskController>/5/Tags
        [HttpGet("{id}/Tags")]
        public ActionResult Tags(int id)
        {
            var task = taskRepository.GetById(id);
            if (task == null)
                return NotFound();
            else
            {
                return new JsonResult(tagRepository.GetByTask(task));
            }
        }*/

        // POST api/<TaskController>
        [HttpPost]
        public ActionResult Post([FromBody] Data.Models.Task value)
        {
            try
            {
                _taskRepository.Add(value);
                //return Created($"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.Path}{HttpContext.Request.QueryString}", value);
                return CreatedAtAction("Post", value);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT api/<TaskController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Data.Models.Task value)
        {
            var task = _taskRepository.GetById(id);
            if (task == null) 
                return NotFound();
            task = value;
            task.Id = id;
            _taskRepository.Update(task);
            return Ok();
        }

        // DELETE api/<TaskController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var task = _taskRepository.GetById(id);
            if (task == null) 
                return NotFound();
            _taskRepository.Delete(id);
            return NoContent();
        }
    }
}
