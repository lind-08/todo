﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoApi.Data.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ToDoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskListsController : ControllerBase
    {
        private readonly ITaskListsRepository _taskLists;
        private readonly ITagsRepository _tagsRepository;

        public TaskListsController(ITaskListsRepository taskLists, ITagsRepository tagsRepository)
        {
            _taskLists = taskLists;
            _tagsRepository = tagsRepository;
        }

        // GET: api/<TasksListsController>
        [HttpGet]
        public ActionResult Get(int? tagId)
        {
            IEnumerable<Data.Models.TaskList> lists;
            if (tagId != null)
            {
                var tag = _tagsRepository.GetById((int)tagId);
                if (tag != null)
                    lists = _taskLists.GetByTag(tag);
                else
                    return NotFound();
            }
            else
            {
                lists = _taskLists.GetAll();
            }
            return new JsonResult(lists);
        }

        // GET api/<TasksListsController>/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var list = _taskLists.GetById(id);
            if (list != null)
            {
                return new JsonResult(list);
            }
            return NotFound();
        }

        // POST api/<TasksListsController>
        [HttpPost]
        public ActionResult Post([FromBody] Data.Models.TaskList value)
        {
            try
            {
                _taskLists.Add(value);
                //return Created($"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.Path}{HttpContext.Request.QueryString}", value);
                return CreatedAtAction("Post", value);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT api/<TasksListsController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Data.Models.TaskList value)
        {
            var list = _taskLists.GetById(id);
            if (list == null) 
                return NotFound();
            list = value;
            list.Id = id;
            _taskLists.Update(list);
            return Ok();
        }

        // DELETE api/<TasksListsController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var list = _taskLists.GetById(id);
            if (list == null) 
                return NotFound();
            _taskLists.Delete(id);
            return NoContent();
        }
    }
}
