﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System.Collections.Generic;
using ToDoApi.Data.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ToDoApi.Controllers
{
    [ApiController]
    [Route("api/tasks/{taskId}/tags")]
    public class TaskTagsController : ControllerBase
    {
        private readonly ITagsRepository _tagsRepository;
        private readonly ITasksRepository _tasksRepository;

        public TaskTagsController(ITasksRepository tasksRepository, ITagsRepository tagsRepository)
        {
            _tagsRepository = tagsRepository;
            _tasksRepository = tasksRepository;
        }

        // GET: api/<TaskController>
        [HttpGet]
        public ActionResult Get(int taskId)
        {
            var task = _tasksRepository.GetById(taskId);
            if (task == null)
                return NotFound();
            var tags = _tagsRepository.GetByTask(task);
            return new JsonResult(tags);
        }

        // PUT api/<TaskController>/5
        [HttpPut("id")]
        public ActionResult Put(int taskId, int tagId)
        {
            var task = _tasksRepository.GetById(taskId);
            if (task == null)
                return NotFound();
            var tag = _tagsRepository.GetById(tagId);
            if (tag == null)
                return NotFound();
            if (!task.Tags.Contains(tag))
            {
               task.Tags.Add(tag);
                _tasksRepository.Update(task);
            }
            return Ok();
        }

        // DELETE api/<TaskController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int taskId, int id)
        {
            var task = _tasksRepository.GetById(taskId);
            if (task == null)
                return NotFound();
            var tag = _tagsRepository.GetById(id);
            if (tag == null)
                return NotFound();
            if (task.Tags.Contains(tag))
            {
                task.Tags.Remove(tag);
                return NoContent();
            }
            return Ok();
        }
    }
}
