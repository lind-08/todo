﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using ToDoApi.Data.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ToDoApi.Controllers
{
    [ApiController]
    [Route("api/taskLists/{taskListId}/tasks")]
    public class TaskListsTasksController : ControllerBase
    {
        private readonly ITaskListsRepository _taskListsRepository;
        private readonly ITasksRepository _tasksRepository;

        public TaskListsTasksController(ITaskListsRepository taskListsRepository, ITasksRepository tasksRepository)
        {
            _taskListsRepository = taskListsRepository;
            _tasksRepository = tasksRepository;
        }

        [HttpGet]
        public ActionResult Get(int taskListId)
        {
            var list = _taskListsRepository.GetById(taskListId);
            if (list == null) 
                return NotFound();
            var tasks = _tasksRepository.GetByTaskList(list);
            return new JsonResult(tasks);
        }

       
        // PUT api/<TaskController>/5
        [HttpPut]
        public ActionResult Put(int taskListId, int taskId)
        {
            var list = _taskListsRepository.GetById(taskListId);
            if (list == null) 
                return NotFound();
            var task = _tasksRepository.GetById(taskId);
            if (task == null) 
                return NotFound();
            if (!list.Tasks.Contains(task))
            {
                list.Tasks.Add(task);
                _taskListsRepository.Update(list);
            }
            return Ok();
        }

        // DELETE api/<TaskController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int taskListId, int id)
        {
            var list = _taskListsRepository.GetById(taskListId);
            if (list == null)
                return NotFound();
            var task = _tasksRepository.GetById(id);
            if (task == null)
                return NotFound();
            if (list.Tasks.Contains(task))
            {
                list.Tasks.Remove(task);
                return NoContent();
            }
            return Ok();
        }
    }
}
