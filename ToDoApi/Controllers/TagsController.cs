﻿using Microsoft.AspNetCore.Mvc;
using System;
using ToDoApi.Data.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ToDoApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TagsController : ControllerBase
    {
        private readonly ITagsRepository _tagsRepository;

        public TagsController(ITagsRepository tagsRepository)
        {
            _tagsRepository = tagsRepository;
        }

        // GET: api/<TaskController>
        [HttpGet]
        public ActionResult Get()
        {
            return new JsonResult(_tagsRepository.GetAll());
        }

        // GET api/<TaskController>/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var tags = _tagsRepository.GetById(id);
            if (tags == null)
                return NotFound();
            return new JsonResult(tags);
        }

        // POST api/<TaskController>
        [HttpPost]
        public ActionResult Post([FromBody] Data.Models.Tag value)
        {
            try
            {
                _tagsRepository.Add(value);
                //return Created($"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.Path}{HttpContext.Request.QueryString}", value);
                return CreatedAtAction("Post", value);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT api/<TaskController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Data.Models.Tag value)
        {
            var tags = _tagsRepository.GetById(id);
            if (tags == null) 
                return NotFound();
            tags = value;
            tags.Id = id;
            _tagsRepository.Update(tags);
            return Ok();

        }

        // DELETE api/<TaskController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var tags = _tagsRepository.GetById(id);
            if (tags == null) 
                return NotFound();
            _tagsRepository.Delete(id);
            return NoContent();
        }
    }
}
