﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoApi.Data;
using ToDoApi.Data.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ToDoApi.Controllers
{
    [ApiController]
    [Route("api/taskLists/{taskListId}/tags")]
    public class TaskListsTagsController : ControllerBase
    {
        private readonly ITaskListsRepository _taskListsRepository;
        private readonly ITagsRepository _tagsRepository;

        public TaskListsTagsController(ITaskListsRepository taskListsRepository, ITagsRepository tagsRepository)
        {
            _taskListsRepository = taskListsRepository;
            _tagsRepository = tagsRepository;
        }

        [HttpGet]
        public ActionResult Get(int taskListId)
        {
            var list = _taskListsRepository.GetById(taskListId);
            if (list == null)
                return NotFound();
            var tags = _tagsRepository.GetByTaskList(list);
            return new JsonResult(tags);
        }

        // Post api/taskLists/{taskListId}/tags/5
        [HttpPut("{id}")]
        public ActionResult Put(int taskListId, int tagId)
        {
            var list = _taskListsRepository.GetById(taskListId);
            if (list == null) 
                return NotFound();
            var tag = _tagsRepository.GetById(tagId);
            if (tag == null) 
                return NotFound();
            if (!list.Tags.Contains(tag))
            {
                list.Tags.Add(tag);
                _taskListsRepository.Update(list);
            }
            return Ok();
        }

        // DELETE api/taskLists/{taskListId}/tags/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int taskListId, int id)
        {
            var list = _taskListsRepository.GetById(taskListId);
            if (list == null) 
                return NotFound();
            var tag = _tagsRepository.GetById(id);
            if (tag == null)
                return NotFound();
            if (!list.Tags.Contains(tag)) 
                return Ok();
            list.Tags.Remove(tag);
            return NoContent();
        }
    }
}
