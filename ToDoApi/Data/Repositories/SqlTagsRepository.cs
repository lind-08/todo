﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Data.Models;

namespace ToDoApi.Data.Repositories
{
    public class SqlTagsRepository : BaseSqlEntityRepository<Models.Tag, ApplicationContext>, ITagsRepository
    {
        public SqlTagsRepository(ApplicationContext context) : base(context)
        {

        }

        public IEnumerable<Tag> GetByTask(Task task)
        {
            return Context.Set<Tag>().Where(p => p.Tasks.Contains(task));
        }

        public IEnumerable<Tag> GetByTaskList(TaskList tasksList)
        {
            return Context.Set<Tag>().Where(p => p.TaskLists.Contains(tasksList));
        }
    }
}
