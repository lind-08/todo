﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Data.Models;


namespace ToDoApi.Data.Repositories
{
    public interface ITaskListsRepository : IEntityRepository<TaskList>
    {
        public IEnumerable<TaskList> GetByTask(Task task);
        public IEnumerable<TaskList> GetByTag(Tag tag);
    }
}
