﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Data;

namespace ToDoApi.Data.Repositories
{
    public interface ITasksRepository : IEntityRepository<Models.Task>
    {
        public IEnumerable<Models.Task> GetByTaskList(Models.TaskList taskList);
        public IEnumerable<Models.Task> GetByTag(Models.Tag tag);
    }
}
