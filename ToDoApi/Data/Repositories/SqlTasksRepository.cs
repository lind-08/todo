﻿using System.Collections.Generic;
using System.Linq;
using ToDoApi.Data.Models;

namespace ToDoApi.Data.Repositories
{
    public class SqlTasksRepository : BaseSqlEntityRepository<Task, ApplicationContext>, ITasksRepository
    {
        public SqlTasksRepository(ApplicationContext context) : base(context)
        {
        }

        public IEnumerable<Task> GetByTag(Tag tag)
        {
            return Context.Set<Task>().Where(p => p.Tags.Contains(tag));
        }

        public IEnumerable<Task> GetByTaskList(TaskList taskList)
        {
            return Context.Set<Task>().Where(p => p.TaskList == taskList);
        }
    }
}
