﻿using System.Collections.Generic;
using System.Linq;
using ToDoApi.Data.Models;

namespace ToDoApi.Data.Repositories
{
    public class SqlTaskListsRepository : BaseSqlEntityRepository<TaskList, ApplicationContext>, ITaskListsRepository
    {
        public SqlTaskListsRepository(ApplicationContext context) : base(context)
        {

        }

        public IEnumerable<TaskList> GetByTag(Tag tag)
        {
            return Context.Set<TaskList>().Where(entity => entity.Tags.Contains(tag));
        }

        public IEnumerable<TaskList> GetByTask(Task task)
        {
            return Context.Set<TaskList>().Where(entity => entity.Tasks.Contains(task));
        }
    }
}
