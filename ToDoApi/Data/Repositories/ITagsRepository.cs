﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Data.Models;

namespace ToDoApi.Data.Repositories
{
    public interface ITagsRepository : IEntityRepository<Tag>
    {
        public IEnumerable<Tag> GetByTask(Task task);
        public IEnumerable<Tag> GetByTaskList(TaskList tasksList);
    }
}
