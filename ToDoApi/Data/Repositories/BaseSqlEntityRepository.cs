﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ToDoApi.Data.Models;

namespace ToDoApi.Data.Repositories
{
    public class BaseSqlEntityRepository<TEntity, TContext> : IEntityRepository<TEntity>
        where TEntity: class, IEntity
        where TContext: DbContext
    {
        protected TContext Context;

        public BaseSqlEntityRepository(TContext context)
        {
            Context = context;
        }

        public async System.Threading.Tasks.Task<TEntity> AddAsync(TEntity entity)
        {
            var res = await Context.AddAsync(entity);
            await Context.SaveChangesAsync();
            return res.Entity;
        }

        public TEntity Add(TEntity entity)
        {
            if (entity.Id == 0)
            {
                var res = Context.Add(entity);
                Context.SaveChanges();
                return res.Entity;
            }
            throw new ArgumentException("Id must be zero");
        }

        public async void Update(TEntity entity)
        {
            Context.Update(entity);
            await Context.SaveChangesAsync();
        }

        public async void Delete(int id)
        {
            var entity = GetById(id);
            if (entity == null) 
                return;
            Context.Remove(entity);
            await Context.SaveChangesAsync();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Context.Set<TEntity>().ToList();
        }

        public TEntity GetById(int id)
        {
            return Context.Set<TEntity>().FirstOrDefault(p => p.Id == id);
        }

    }
}
