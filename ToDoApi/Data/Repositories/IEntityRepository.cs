﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ToDoApi.Data.Repositories
{
    public interface IEntityRepository<T> 
        where T : Models.IEntity
    {
        public T Add(T entity);
        public Task<T> AddAsync(T entity);
        public void Update(T entity);
        public void Delete(int id);

        public T GetById(int id);
        public IEnumerable<T> GetAll();
    }
}
