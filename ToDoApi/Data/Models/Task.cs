﻿using System;
using System.Collections.Generic;

namespace ToDoApi.Data.Models
{
    public class Task : IEntity
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Comment { get; set; }
        public bool Status { get; set; }
        public DateTime CreateDate { get; set; }

        public TaskList TaskList { get; set; }

        public List<Tag> Tags { get; set; }
    }
}
