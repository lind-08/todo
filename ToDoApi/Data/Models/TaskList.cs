﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ToDoApi.Data.Models
{
    public class TaskList : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public string Comment { get; set; }

        public List<Task> Tasks { get; set; }
        public List<Tag> Tags { get; set; }

    }
}
