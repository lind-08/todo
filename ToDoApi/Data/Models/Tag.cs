﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoApi.Data.Models
{
    public class Tag : IEntity
    {
        public int Id { get; set; }
        public string Text { get; set; }

        public List<Task> Tasks { get; set; }
        public List<TaskList> TaskLists { get; set; }
    }
}
