﻿using Microsoft.EntityFrameworkCore;
using ToDoApi.Data.Models;

namespace ToDoApi.Data
{
    public sealed class ApplicationContext : DbContext
    {
        DbSet<Task> Tasks { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) 
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
