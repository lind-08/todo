﻿using System;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Controllers;
using ToDoApi.Data.Models;
using ToDoApi.Data.Repositories;
using Xunit;

namespace ToDoApiTest.Controllers
{
    public class TaskListsControllerTest
    {
        [Fact]
        public void GetTest()
        {
            var mockTaskListsRep = new Mock<ITaskListsRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tagsList = new List<Tag> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            var taskLists = new List<TaskList> { 
                new() { Id = 1, Tags = new List<Tag>{tagsList[0], tagsList[1]}},
                new() { Id = 2, Tags = new List<Tag>{tagsList[1], tagsList[2]}}
            };

            mockTaskListsRep.Setup(mock => mock.GetAll()).Returns(taskLists);

            var taskListsController = new TaskListsController(mockTaskListsRep.Object, mockTagsRep.Object);
            
            var result = taskListsController.Get(null);
            mockTaskListsRep.Verify(mock => mock.GetAll());
            var jsonResult = Assert.IsType<JsonResult>(result);
            var lists = (IEnumerable<TaskList>)jsonResult.Value;
            var enumerable = lists.ToList();
            Assert.NotEmpty(enumerable);
            Assert.Equal(2, enumerable.Count);
            Assert.Contains(enumerable, x => x.Id == 1);
            Assert.Contains(enumerable, x => x.Id == 2);

            mockTaskListsRep.Reset();
            mockTaskListsRep.Setup(mock => mock.GetByTag(It.IsAny<Tag>())).Returns((Tag i) => taskLists.Where(x => x.Tags.Contains(i)));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));

            result = taskListsController.Get((int?)1);
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListsRep.Verify(mock => mock.GetByTag(It.IsAny<Tag>()));
            jsonResult = Assert.IsType<JsonResult>(result);
            lists = (IEnumerable<TaskList>)jsonResult.Value;
            var collection = lists.ToList();
            Assert.NotEmpty(collection);
            Assert.Single(collection);
            Assert.Contains(collection, x => x.Id == 1);

            mockTaskListsRep.Reset();
            mockTaskListsRep.Setup(mock => mock.GetByTag(It.IsAny<Tag>())).Returns((Tag i) => taskLists.Where(x => x.Tags.Contains(i)));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            
            result = taskListsController.Get((int?)4);
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListsRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GetByIdTest()
        {
            var mockTaskListsRep = new Mock<ITaskListsRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tasksList = new List<TaskList> {
                new() { Id = 1 },
                new() { Id = 2 }
            };
            mockTaskListsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));

            var taskListsController = new TaskListsController(mockTaskListsRep.Object, mockTagsRep.Object);
            
            var result = taskListsController.Get(1);
            mockTaskListsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.VerifyNoOtherCalls();
            var jsonResult = Assert.IsType<JsonResult>(result);
            var taskList = (TaskList) jsonResult.Value;
            Assert.NotNull(taskList);
            Assert.Equal(1, taskList.Id);

            mockTaskListsRep.Reset();
            mockTaskListsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));

            result = taskListsController.Get(4);
            mockTaskListsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void PostTest()
        {
            var mockId = 1;
            var mockTaskListsRep = new Mock<ITaskListsRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            mockTaskListsRep.Setup(mock => mock.Add(It.IsAny<TaskList>())).Returns((TaskList x) => { 
                x.Id = mockId++; 
                return x; 
            } );
            mockTaskListsRep.Setup(mock => mock.Add(It.Is<TaskList>(x => x.Id != 0))).Throws(new ArgumentException("Id must be zero"));

           var taskListsController = new TaskListsController(mockTaskListsRep.Object, mockTagsRep.Object);

            var taskList = new TaskList { Name = "New list" };

            var result = taskListsController.Post(taskList);
            mockTaskListsRep.Verify(mock => mock.Add(It.IsAny<TaskList>()));
            var createdResult = Assert.IsType<CreatedAtActionResult>(result);
            var createdResultValue = (TaskList)createdResult.Value;
            Assert.NotNull(createdResultValue);
            Assert.Equal(1, createdResultValue.Id);

            taskList = new TaskList { Id = 1, Name = "Another list" };
            result = taskListsController.Post(taskList);
            mockTaskListsRep.Verify(mock => mock.Add(It.Is<TaskList>(x => x.Id != 0)));
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public void PutTest()
        {
            var mockTaskListsRep = new Mock<ITaskListsRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var taskLists = new List<TaskList> {
                new() { Id = 1, Name  = "first" },
                new() { Id = 2 , Name = "second"}
            };
            mockTaskListsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => taskLists.FirstOrDefault(x => x.Id == i));
            mockTaskListsRep.Setup(mock => mock.Update(It.IsAny<TaskList>()));
            
            var taskListsController = new TaskListsController(mockTaskListsRep.Object, mockTagsRep.Object);

            var taskList = new TaskList { Name = "New List" };

            var result = taskListsController.Put(1, taskList);
            mockTaskListsRep.VerifyAll();
            Assert.IsType<OkResult>(result);

            mockTaskListsRep.Reset();
            mockTaskListsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => taskLists.FirstOrDefault(x => x.Id == i));
            mockTaskListsRep.Setup(mock => mock.Update(It.IsAny<TaskList>()));

            result = taskListsController.Put(6, taskList);
            mockTaskListsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListsRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void DeleteTest()
        {
            var mockTaskListsRep = new Mock<ITaskListsRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tasksList = new List<TaskList> {
                new() { Id = 1, Name  = "first" },
                new() { Id = 2 , Name = "second"}
            };
            mockTaskListsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));
            mockTaskListsRep.Setup(mock => mock.Delete(It.IsAny<int>()));

            var taskListsController = new TaskListsController(mockTaskListsRep.Object, mockTagsRep.Object);

            var result = taskListsController.Delete(1);
            mockTaskListsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListsRep.Verify(mock => mock.Delete(It.IsAny<int>()));
            Assert.IsType<NoContentResult>(result);

            mockTaskListsRep.Reset();
            mockTaskListsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));
            mockTaskListsRep.Setup(mock => mock.Delete(It.IsAny<int>()));

            result = taskListsController.Delete(6);
            mockTaskListsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListsRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
