﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Controllers;
using ToDoApi.Data.Models;
using ToDoApi.Data.Repositories;
using Xunit;

namespace ToDoApiTest.Controllers
{
    public class TaskListsTasksControllerTest
    {
        [Fact]
        public void GetTest()
        {
            var mockTaskListsRep = new Mock<ITaskListsRepository>();
            var mockTasksRep = new Mock<ITasksRepository>();
            var tasksList = new List<Task> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            var taskLists = new List<TaskList> { 
                new() { Id = 1, Tasks = new List<Task>{tasksList[0], tasksList[1]}},
                new() { Id = 2, Tasks = new List<Task>{tasksList[1], tasksList[2]}}
            };


            mockTaskListsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int id) => taskLists.FirstOrDefault(x => x.Id == id));
            mockTasksRep.Setup(mock => mock.GetByTaskList(It.IsAny<TaskList>())).Returns((TaskList t) =>
            {
                var taskList = taskLists.First(x => x.Id == t.Id);
                return taskList.Tasks;
            });

            var taskListsTagsController = new TaskListsTasksController(mockTaskListsRep.Object, mockTasksRep.Object);
            
            var result = taskListsTagsController.Get(1);
            mockTaskListsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.Verify(mock => mock.GetByTaskList(It.IsAny<TaskList>()));
            var jsonResult = Assert.IsType<JsonResult>(result);
            var tasks = (IEnumerable<Task>)jsonResult.Value;
            var enumerable = tasks.ToList();
            Assert.NotEmpty(enumerable);
            Assert.Equal(2, enumerable.Count);
            Assert.Contains(enumerable, x => x.Text == "First");
            Assert.Contains(enumerable, x => x.Text == "Second");

            mockTaskListsRep.Reset();
            mockTasksRep.Reset();
            mockTaskListsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int id) => taskLists.FirstOrDefault(x => x.Id == id));
            mockTasksRep.Setup(mock => mock.GetByTaskList(It.IsAny<TaskList>())).Returns((TaskList t) =>
            {
                var taskList = taskLists.First(x => x.Id == t.Id);
                return taskList.Tasks;
            });

            result = taskListsTagsController.Get(3);
            mockTaskListsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);
        }
        
        [Fact]
        public void PutTest()
        {
            var mockTaskListRep = new Mock<ITaskListsRepository>();
            var mockTasksRep = new Mock<ITasksRepository>();
            var tasks = new List<Task> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            var taskLists = new List<TaskList> { 
                new() { Id = 1, Tasks = new List<Task>{tasks[0], tasks[1]}},
                new() { Id = 2, Tasks = new List<Task>{tasks[1], tasks[2]}}
            };

            mockTaskListRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int id) => taskLists.FirstOrDefault(x => x.Id == id));
            mockTaskListRep.Setup(mock => mock.Update(It.IsAny<TaskList>()));
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasks.FirstOrDefault(x => x.Id == i));
            var taskListsTagsController = new TaskListsTasksController(mockTaskListRep.Object, mockTasksRep.Object);

            var result = taskListsTagsController.Put(1, 3);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListRep.Verify(mock => mock.Update(It.IsAny<TaskList>()));
            Assert.IsType<OkResult>(result);
            var list = taskLists[0];
            Assert.NotNull(list.Tasks);
            Assert.Equal(3, list.Tasks.Count);
            Assert.Contains(list.Tasks, x => x.Text == "Third");

            mockTaskListRep.Reset();
            mockTasksRep.Reset();
            mockTaskListRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int id) => taskLists.FirstOrDefault(x => x.Id == id));
            mockTaskListRep.Setup(mock => mock.Update(It.IsAny<TaskList>()));
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasks.FirstOrDefault(x => x.Id == i));


            result = taskListsTagsController.Put(3,1);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListRep.VerifyNoOtherCalls();
            mockTasksRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);

            result = taskListsTagsController.Put(1, 4);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListRep.VerifyNoOtherCalls();
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void DeleteTest()
        {
            var mockTaskListRep = new Mock<ITaskListsRepository>();
            var mockTasksRep = new Mock<ITasksRepository>();
            var tasks = new List<Task> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            var taskLists = new List<TaskList> { 
                new() { Id = 1, Tasks = new List<Task>{tasks[0], tasks[1]}},
                new() { Id = 2, Tasks = new List<Task>{tasks[1], tasks[2]}}
            };

            mockTaskListRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => taskLists.FirstOrDefault(x => x.Id == i));
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasks.FirstOrDefault(x => x.Id == i));
            
            var taskListsTasksController = new TaskListsTasksController(mockTaskListRep.Object, mockTasksRep.Object);

            var result = taskListsTasksController.Delete(1,2);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<NoContentResult>(result);
            mockTaskListRep.Reset();
            mockTaskListRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => taskLists.FirstOrDefault(x => x.Id == i));
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasks.FirstOrDefault(x => x.Id == i));
            
            result = taskListsTasksController.Delete(3,2);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);

            mockTaskListRep.Reset();
            mockTaskListRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => taskLists.FirstOrDefault(x => x.Id == i));
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasks.FirstOrDefault(x => x.Id == i));
            
            result = taskListsTasksController.Delete(1,4);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<NotFoundResult>(result);
            
            mockTaskListRep.Reset();
            mockTaskListRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => taskLists.FirstOrDefault(x => x.Id == i));
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasks.FirstOrDefault(x => x.Id == i));

            result = taskListsTasksController.Delete(1,2);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<OkResult>(result);
        }
    }
}
