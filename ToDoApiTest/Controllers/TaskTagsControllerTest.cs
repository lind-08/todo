﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Controllers;
using ToDoApi.Data.Models;
using ToDoApi.Data.Repositories;
using Xunit;

namespace ToDoApiTest.Controllers
{
    public class TaskTagsControllerTest
    {
        [Fact]
        public void GetTest()
        {
            var mockTasksRep = new Mock<ITasksRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tagsList = new List<Tag> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            var tasksList = new List<Task> { 
                new() { Id = 1, Tags = new List<Tag>{tagsList[0], tagsList[1]}},
                new() { Id = 2, Tags = new List<Tag>{tagsList[1], tagsList[2]}}
            };


            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int id) => tasksList.FirstOrDefault(x => x.Id == id));
            mockTagsRep.Setup(mock => mock.GetByTask(It.IsAny<Task>())).Returns((Task t) =>
            {
                var task = tasksList.First(x => x.Id == t.Id);
                return task.Tags;
            });

            var tasksTagsController = new TaskTagsController(mockTasksRep.Object, mockTagsRep.Object);
            
            var result = tasksTagsController.Get(1);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.Verify(mock => mock.GetByTask(It.IsAny<Task>()));
            var jsonResult = Assert.IsType<JsonResult>(result);
            var tags = (IEnumerable<Tag>)jsonResult.Value;
            var enumerable = tags.ToList();
            Assert.NotEmpty(enumerable);
            Assert.Equal(2, enumerable.Count);
            Assert.Contains(enumerable, x => x.Text == "First");
            Assert.Contains(enumerable, x => x.Text == "Second");

            mockTasksRep.Reset();
            mockTagsRep.Reset();
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int id) => tasksList.FirstOrDefault(x => x.Id == id));
            mockTagsRep.Setup(mock => mock.GetByTask(It.IsAny<Task>())).Returns((Task t) =>
            {
                var task = tasksList.First(x => x.Id == t.Id);
                return task.Tags;
            });

            result = tasksTagsController.Get(3);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);
        }
        
        [Fact]
        public void PutTest()
        {
            var mockTasksRep = new Mock<ITasksRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tagsList = new List<Tag> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            var tasksList = new List<Task> { 
                new() { Id = 1, Tags = new List<Tag>{tagsList[0], tagsList[1]}},
                new() { Id = 2, Tags = new List<Tag>{tagsList[1], tagsList[2]}}
            };

            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int id) => tasksList.FirstOrDefault(x => x.Id == id));
            mockTasksRep.Setup(mock => mock.Update(It.IsAny<Task>()));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            var taskTagsController = new TaskTagsController(mockTasksRep.Object, mockTagsRep.Object);

            var result = taskTagsController.Put(1, 3);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.Verify(mock => mock.Update(It.IsAny<Task>()));
            Assert.IsType<OkResult>(result);
            var task = tasksList[0];
            Assert.NotNull(task.Tags);
            Assert.Equal(3, task.Tags.Count);
            Assert.Contains(task.Tags, x => x.Text == "Third");

            mockTasksRep.Reset();
            mockTagsRep.Reset();
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int id) => tasksList.FirstOrDefault(x => x.Id == id));
            mockTasksRep.Setup(mock => mock.Update(It.IsAny<Task>()));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));


            result = taskTagsController.Put(3,1);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.VerifyNoOtherCalls();
            mockTagsRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);

            result = taskTagsController.Put(1, 4);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.VerifyNoOtherCalls();
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void DeleteTest()
        {
            var mockTasksRep = new Mock<ITasksRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tagsList = new List<Tag> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            var tasksList = new List<Task> { 
                new() { Id = 1, Tags = new List<Tag>{tagsList[0], tagsList[1]}},
                new() { Id = 2, Tags = new List<Tag>{tagsList[1], tagsList[2]}}
            };

            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            
            var taskTagsController = new TaskTagsController(mockTasksRep.Object, mockTagsRep.Object);

            var result = taskTagsController.Delete(1,2);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<NoContentResult>(result);
            mockTasksRep.Reset();
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            
            result = taskTagsController.Delete(3,2);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);

            mockTasksRep.Reset();
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            
            result = taskTagsController.Delete(1,4);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<NotFoundResult>(result);
            
            mockTasksRep.Reset();
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));

            result = taskTagsController.Delete(1,2);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<OkResult>(result);
        }
    }
}
