﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Controllers;
using ToDoApi.Data.Models;
using ToDoApi.Data.Repositories;
using Xunit;

namespace ToDoApiTest.Controllers
{
    public class TagsControllerTest
    {
        [Fact]
        public void GetTest()
        {
            var mockTagsRep = new Mock<ITagsRepository>();
            var tagsList = new List<Tag> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            mockTagsRep.Setup(mock => mock.GetAll()).Returns(tagsList);

            var tagsController = new TagsController(mockTagsRep.Object);
            
            var result = tagsController.Get();
            mockTagsRep.Verify(mock => mock.GetAll());
            var jsonResult = Assert.IsType<JsonResult>(result);
            var tags = (List<Tag>)jsonResult.Value;
            Assert.NotEmpty(tags);
            Assert.Equal(3, tags.Count);
            Assert.Contains(tags, x => x.Text == "First");
            Assert.Contains(tags, x => x.Text == "Second");
            Assert.Contains(tags, x => x.Text == "Third");
        }

        [Fact]
        public void GetByIdTest()
        {
            var mockTagsRep = new Mock<ITagsRepository>();
            var tagsList = new List<Tag> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i)).Verifiable();

            var tagsController = new TagsController(mockTagsRep.Object);
            
            var result = tagsController.Get(1);
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            var jsonResult = Assert.IsType<JsonResult>(result);
            var tag = (Tag) jsonResult.Value;
            Assert.NotNull(tag);
            Assert.Equal(1, tag.Id);
            Assert.Equal("First", tag.Text);

            mockTagsRep.Reset();
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i)).Verifiable();

            result = tagsController.Get(4);
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            _ = Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void PostTest()
        {
            var mockId = 1;
            var mockTagsRep = new Mock<ITagsRepository>();
            mockTagsRep.Setup(mock => mock.Add(It.IsAny<Tag>())).Returns((Tag x) => { 
                x.Id = mockId++; 
                return x; 
            } ).Verifiable();
            mockTagsRep.Setup(mock => mock.Add(It.Is<Tag>(x => x.Id != 0))).Throws(new ArgumentException("Id must be zero"));

            var tagsController = new TagsController(mockTagsRep.Object);

            var tag = new Tag { Text = "New tag" };

            var result = tagsController.Post(tag);
            mockTagsRep.Verify(mock => mock.Add(It.IsAny<Tag>()));
            var createdResult = Assert.IsType<CreatedAtActionResult>(result);
            var createdTag = (Tag)createdResult.Value;
            Assert.NotNull(createdTag);
            Assert.Equal(1, createdTag.Id);

            tag = new Tag { Id = 1, Text = "New tag" };
            result = tagsController.Post(tag);
            mockTagsRep.Verify(mock => mock.Add(It.Is<Tag>(x => x.Id != 0)));
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public void PutTest()
        {
            var mockTagsRep = new Mock<ITagsRepository>();
            var tagsList = new List<Tag> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            mockTagsRep.Setup(mock => mock.Update(It.IsAny<Tag>()));
            
            var tagsController = new TagsController(mockTagsRep.Object);

            var tag = new Tag { Text = "New tag" };

            var result = tagsController.Put(1, tag);
            mockTagsRep.VerifyAll();
            Assert.IsType<OkResult>(result);

            mockTagsRep.Reset();
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            mockTagsRep.Setup(mock => mock.Update(It.IsAny<Tag>()));

            result = tagsController.Put(6, tag);
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void DeleteTest()
        {
            var mockTagsRep = new Mock<ITagsRepository>();
            var tagsList = new List<Tag> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            mockTagsRep.Setup(mock => mock.Delete(It.IsAny<int>()));

            var tagsController = new TagsController(mockTagsRep.Object);

            var result = tagsController.Delete(1);
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.Verify(mock => mock.Delete(It.IsAny<int>()));
            Assert.IsType<NoContentResult>(result);

            mockTagsRep.Reset();
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            mockTagsRep.Setup(mock => mock.Delete(It.IsAny<int>()));

            result = tagsController.Delete(6);
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
