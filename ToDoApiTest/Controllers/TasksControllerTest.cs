﻿using System;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Controllers;
using ToDoApi.Data.Models;
using ToDoApi.Data.Repositories;
using Xunit;

namespace ToDoApiTest.Controllers
{
    public class TasksControllerTest
    {
        [Fact]
        public void GetTest()
        {
            var mockTasksRep = new Mock<ITasksRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tagsList = new List<Tag> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            var tasksList = new List<Task> { 
                new() { Id = 1, Tags = new List<Tag>{tagsList[0], tagsList[1]}},
                new() { Id = 2, Tags = new List<Tag>{tagsList[1], tagsList[2]}}
            };

            mockTasksRep.Setup(mock => mock.GetAll()).Returns(tasksList);

            var tasksController = new TasksController(mockTasksRep.Object, mockTagsRep.Object);
            
            var result = tasksController.Get(null);
            mockTasksRep.Verify(mock => mock.GetAll());
            var jsonResult = Assert.IsType<JsonResult>(result);
            var tasks = (IEnumerable<Task>)jsonResult.Value;
            var enumerable = tasks.ToList();
            Assert.NotEmpty(enumerable);
            Assert.Equal(2, enumerable.Count);
            Assert.Contains(enumerable, x => x.Id == 1);
            Assert.Contains(enumerable, x => x.Id == 2);

            mockTasksRep.Reset();
            mockTasksRep.Setup(mock => mock.GetByTag(It.IsAny<Tag>())).Returns((Tag i) => tasksList.Where(x => x.Tags.Contains(i)));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));

            result = tasksController.Get((int?)1);
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.Verify(mock => mock.GetByTag(It.IsAny<Tag>()));
            jsonResult = Assert.IsType<JsonResult>(result);
            tasks = (IEnumerable<Task>)jsonResult.Value;
            var collection = tasks.ToList();
            Assert.NotEmpty(collection);
            Assert.Single(collection);
            Assert.Contains(collection, x => x.Id == 1);

            mockTasksRep.Reset();
            mockTasksRep.Setup(mock => mock.GetByTag(It.IsAny<Tag>())).Returns((Tag i) => tasksList.Where(x => x.Tags.Contains(i)));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            
            result = tasksController.Get((int?)4);
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GetByIdTest()
        {
            var mockTasksRep = new Mock<ITasksRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tasksList = new List<Task> {
                new() { Id = 1 },
                new() { Id = 2 }
            };
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));

            var tasksController = new TasksController(mockTasksRep.Object, mockTagsRep.Object);
            
            var result = tasksController.Get(1);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.VerifyNoOtherCalls();
            var jsonResult = Assert.IsType<JsonResult>(result);
            var task = (Task) jsonResult.Value;
            Assert.NotNull(task);
            Assert.Equal(1, task.Id);

            mockTasksRep.Reset();
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));

            result = tasksController.Get(4);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void PostTest()
        {
            var mockId = 1;
            var mockTasksRep = new Mock<ITasksRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            mockTasksRep.Setup(mock => mock.Add(It.IsAny<Task>())).Returns((Task x) => { 
                x.Id = mockId++; 
                return x; 
            } );
            mockTasksRep.Setup(mock => mock.Add(It.Is<Task>(x => x.Id != 0))).Throws(new ArgumentException("Id must be zero"));

           var tasksController = new TasksController(mockTasksRep.Object, mockTagsRep.Object);

            var task = new Task { Text = "New task" };

            var result = tasksController.Post(task);
            mockTasksRep.Verify(mock => mock.Add(It.IsAny<Task>()));
            var createdResult = Assert.IsType<CreatedAtActionResult>(result);
            var createdResultValue = (Task)createdResult.Value;
            Assert.NotNull(createdResultValue);
            Assert.Equal(1, createdResultValue.Id);

            task = new Task { Id = 1, Text = "New task" };
            result = tasksController.Post(task);
            mockTasksRep.Verify(mock => mock.Add(It.Is<Task>(x => x.Id != 0)));
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public void PutTest()
        {
            var mockTasksRep = new Mock<ITasksRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tasksList = new List<Task> {
                new() { Id = 1, Text  = "first" },
                new() { Id = 2 , Text = "second"}
            };
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));
            mockTasksRep.Setup(mock => mock.Update(It.IsAny<Task>()));
            
            var tasksController = new TasksController(mockTasksRep.Object, mockTagsRep.Object);

            var task = new Task { Text = "New task" };

            var result = tasksController.Put(1, task);
            mockTasksRep.VerifyAll();
            Assert.IsType<OkResult>(result);

            mockTasksRep.Reset();
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));
            mockTasksRep.Setup(mock => mock.Update(It.IsAny<Task>()));

            result = tasksController.Put(6, task);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void DeleteTest()
        {
            var mockTasksRep = new Mock<ITasksRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tasksList = new List<Task> {
                new() { Id = 1, Text  = "first" },
                new() { Id = 2 , Text = "second"}
            };
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));
            mockTasksRep.Setup(mock => mock.Delete(It.IsAny<int>()));

            var tasksController = new TasksController(mockTasksRep.Object, mockTagsRep.Object);

            var result = tasksController.Delete(1);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.Verify(mock => mock.Delete(It.IsAny<int>()));
            Assert.IsType<NoContentResult>(result);

            mockTasksRep.Reset();
            mockTasksRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tasksList.FirstOrDefault(x => x.Id == i));
            mockTasksRep.Setup(mock => mock.Delete(It.IsAny<int>()));

            result = tasksController.Delete(6);
            mockTasksRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTasksRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
