﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Controllers;
using ToDoApi.Data.Models;
using ToDoApi.Data.Repositories;
using Xunit;

namespace ToDoApiTest.Controllers
{
    public class TaskListsTagsControllerTest
    {
        [Fact]
        public void GetTest()
        {
            var mockTaskListsRep = new Mock<ITaskListsRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tagsList = new List<Tag> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            var taskLists = new List<TaskList> { 
                new() { Id = 1, Tags = new List<Tag>{tagsList[0], tagsList[1]}},
                new() { Id = 2, Tags = new List<Tag>{tagsList[1], tagsList[2]}}
            };


            mockTaskListsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int id) => taskLists.FirstOrDefault(x => x.Id == id));
            mockTagsRep.Setup(mock => mock.GetByTaskList(It.IsAny<TaskList>())).Returns((TaskList t) =>
            {
                var taskList = taskLists.First(x => x.Id == t.Id);
                return taskList.Tags;
            });

            var taskListsTagsController = new TaskListsTagsController(mockTaskListsRep.Object, mockTagsRep.Object);
            
            var result = taskListsTagsController.Get(1);
            mockTaskListsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.Verify(mock => mock.GetByTaskList(It.IsAny<TaskList>()));
            var jsonResult = Assert.IsType<JsonResult>(result);
            var tags = (IEnumerable<Tag>)jsonResult.Value;
            var enumerable = tags.ToList();
            Assert.NotEmpty(enumerable);
            Assert.Equal(2, enumerable.Count);
            Assert.Contains(enumerable, x => x.Text == "First");
            Assert.Contains(enumerable, x => x.Text == "Second");

            mockTaskListsRep.Reset();
            mockTagsRep.Reset();
            mockTaskListsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int id) => taskLists.FirstOrDefault(x => x.Id == id));
            mockTagsRep.Setup(mock => mock.GetByTaskList(It.IsAny<TaskList>())).Returns((TaskList t) =>
            {
                var taskList = taskLists.First(x => x.Id == t.Id);
                return taskList.Tags;
            });

            result = taskListsTagsController.Get(3);
            mockTaskListsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);
        }
        
        [Fact]
        public void PutTest()
        {
            var mockTaskListRep = new Mock<ITaskListsRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tagsList = new List<Tag> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            var taskLists = new List<TaskList> { 
                new() { Id = 1, Tags = new List<Tag>{tagsList[0], tagsList[1]}},
                new() { Id = 2, Tags = new List<Tag>{tagsList[1], tagsList[2]}}
            };

            mockTaskListRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int id) => taskLists.FirstOrDefault(x => x.Id == id));
            mockTaskListRep.Setup(mock => mock.Update(It.IsAny<TaskList>()));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            var taskListsTagsController = new TaskListsTagsController(mockTaskListRep.Object, mockTagsRep.Object);

            var result = taskListsTagsController.Put(1, 3);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListRep.Verify(mock => mock.Update(It.IsAny<TaskList>()));
            Assert.IsType<OkResult>(result);
            var taskList = taskLists[0];
            Assert.NotNull(taskList.Tags);
            Assert.Equal(3, taskList.Tags.Count);
            Assert.Contains(taskList.Tags, x => x.Text == "Third");

            mockTaskListRep.Reset();
            mockTagsRep.Reset();
            mockTaskListRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int id) => taskLists.FirstOrDefault(x => x.Id == id));
            mockTaskListRep.Setup(mock => mock.Update(It.IsAny<TaskList>()));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));


            result = taskListsTagsController.Put(3,1);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListRep.VerifyNoOtherCalls();
            mockTagsRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);

            result = taskListsTagsController.Put(1, 4);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListRep.VerifyNoOtherCalls();
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void DeleteTest()
        {
            var mockTaskListRep = new Mock<ITaskListsRepository>();
            var mockTagsRep = new Mock<ITagsRepository>();
            var tagsList = new List<Tag> {
                new() { Id = 1, Text = "First" },
                new() { Id = 2, Text = "Second" },
                new() { Id = 3, Text = "Third" } };
            var taskLists = new List<TaskList> { 
                new() { Id = 1, Tags = new List<Tag>{tagsList[0], tagsList[1]}},
                new() { Id = 2, Tags = new List<Tag>{tagsList[1], tagsList[2]}}
            };

            mockTaskListRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => taskLists.FirstOrDefault(x => x.Id == i));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            
            var taskListsTagsController = new TaskListsTagsController(mockTaskListRep.Object, mockTagsRep.Object);

            var result = taskListsTagsController.Delete(1,2);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<NoContentResult>(result);
            mockTaskListRep.Reset();
            mockTaskListRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => taskLists.FirstOrDefault(x => x.Id == i));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            
            result = taskListsTagsController.Delete(3,2);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTaskListRep.VerifyNoOtherCalls();
            Assert.IsType<NotFoundResult>(result);

            mockTaskListRep.Reset();
            mockTaskListRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => taskLists.FirstOrDefault(x => x.Id == i));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));
            
            result = taskListsTagsController.Delete(1,4);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<NotFoundResult>(result);
            
            mockTaskListRep.Reset();
            mockTaskListRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => taskLists.FirstOrDefault(x => x.Id == i));
            mockTagsRep.Setup(mock => mock.GetById(It.IsAny<int>())).Returns((int i) => tagsList.FirstOrDefault(x => x.Id == i));

            result = taskListsTagsController.Delete(1,2);
            mockTaskListRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            mockTagsRep.Verify(mock => mock.GetById(It.IsAny<int>()));
            Assert.IsType<OkResult>(result);
        }
    }
}
