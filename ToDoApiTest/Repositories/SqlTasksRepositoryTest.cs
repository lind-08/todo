﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Data;
using ToDoApi.Data.Models;
using ToDoApi.Data.Repositories;
using Xunit;

namespace ToDoApiTest.Repositories
{
    public class SqlTasksRepositoryTest
    {
        protected DbContextOptions<ApplicationContext> ContextOptions { get; }

        public SqlTasksRepositoryTest()
        {
            this.ContextOptions = new DbContextOptionsBuilder<ApplicationContext>()
                .UseSqlite("Filename=Test1.db")
                .Options;
            Seed();
        }

        private void Seed()
        {
            using var context = new ApplicationContext(ContextOptions);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            var taskList1 = new TaskList { Name = "taskList1" };
            var taskList2 = new TaskList { Name = "taskList2" };

            var entity = new Tag { Text = "first" };
            var entity1 = new Tag { Text = "second" };

            var task11 = new Task { Text = "task11", TaskList = taskList1, Tags = new List<Tag> { entity } };
            var task12 = new Task { Text = "task12", TaskList = taskList1, Tags = new List<Tag> { entity1 } };
            var task21 = new Task { Text = "task21", TaskList = taskList2, Tags = new List<Tag> { entity } };
            var task22 = new Task { Text = "task22", TaskList = taskList2, Tags = new List<Tag> { entity1 } };

            context.AddRange(taskList1, taskList2);
            context.AddRange(entity, entity1);
            context.AddRange(task11, task12, task21, task22);
            context.SaveChanges();
        }

        [Fact]
        public void GetByTaskListTest()
        {
            using var context = new ApplicationContext(ContextOptions);
            var repository = new SqlTasksRepository(context);
            var taskList1 = new TaskList { Id = 1, Name = "taskList1" };
            var tasks = repository.GetByTaskList(taskList1).ToList();
            Assert.NotEmpty(tasks);
            Assert.Contains(tasks, x => x.Text == "task11");
            Assert.Contains(tasks, x => x.Text == "task12");
            var taskList2 = new TaskList { Id = 2, Name = "taskList2" };
            var tasks1 = repository.GetByTaskList(taskList2).ToList();
            Assert.NotEmpty(tasks1);
            Assert.Contains(tasks1, x => x.Text == "task21");
            Assert.Contains(tasks1, x => x.Text == "task22");
        }

        [Fact]
        public void GetByTagsTest()
        {
            using var context = new ApplicationContext(ContextOptions);
            var repository = new SqlTasksRepository(context);
            var tag1 = new Tag { Id = 1, Text = "first" };
            var tasks = repository.GetByTag(tag1).ToList();
            Assert.NotEmpty(tasks);
            Assert.Contains(tasks, x => x.Text == "task11");
            Assert.Contains(tasks, x => x.Text == "task21");
            var tag2 = new Tag { Id = 2, Text = "second" };
            var tasks1 = repository.GetByTag(tag2).ToList();
            Assert.NotEmpty(tasks1);
            Assert.Contains(tasks1, x => x.Text == "task12");
            Assert.Contains(tasks1, x => x.Text == "task22");
        }
    }
}
