﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Data;
using ToDoApi.Data.Models;
using ToDoApi.Data.Repositories;
using Xunit;

namespace ToDoApiTest.Repositories
{
    public class SqlTaskListsRepositoryTest
    {
        protected DbContextOptions<ApplicationContext> ContextOptions { get; }

        public SqlTaskListsRepositoryTest()
        {
            this.ContextOptions = new DbContextOptionsBuilder<ApplicationContext>()
                .UseSqlite("Filename=TaskListsTest.db")
                .Options;
            Seed();
        }

        private void Seed()
        {
            using var context = new ApplicationContext(ContextOptions);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
                
            var entity = new Tag { Text = "first" };
            var entity1 = new Tag { Text = "second" };
            var entity2 = new Tag { Text = "third" };
            var entity3 = new Tag { Text = "firth" };

            var taskList1 = new TaskList { Name = "taskList1", Tags = new List<Tag> { entity, entity2 } };
            var taskList2 = new TaskList { Name = "taskList2", Tags = new List<Tag> { entity1, entity3 } };
            var taskList3 = new TaskList { Name = "taskList3", Tags = new List<Tag> { entity, entity1 } };
            var taskList4 = new TaskList { Name = "taskList4", Tags = new List<Tag> { entity2, entity3 } };

            context.AddRange(entity, entity1);
            context.AddRange(taskList1, taskList2, taskList3, taskList4);
            context.SaveChanges();
        }

        [Fact]
        public void GetByTagTest()
        {
            using var context = new ApplicationContext(ContextOptions);
            var repository = new SqlTaskListsRepository(context);
            var tag = new Tag{ Id = 1 };
            var taskLists = repository.GetByTag(tag).ToList();
            Assert.NotEmpty(taskLists);
            Assert.Contains(taskLists, x => x.Name == "taskList1");
            Assert.Contains(taskLists, x => x.Name == "taskList3");
            var tag1 = new Tag { Id = 2 };
            var taskLists1 = repository.GetByTag(tag1).ToList();
            Assert.NotEmpty(taskLists1);
            Assert.Contains(taskLists1, x => x.Name == "taskList2");
            Assert.Contains(taskLists1, x => x.Name == "taskList3");
        }
    }
}
