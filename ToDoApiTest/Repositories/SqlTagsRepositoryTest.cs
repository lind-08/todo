﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using ToDoApi.Data;
using ToDoApi.Data.Models;
using ToDoApi.Data.Repositories;
using Xunit;

namespace ToDoApiTest.Repositories
{
    public class SqlTagsRepositoryTest
    {
        protected DbContextOptions<ApplicationContext> ContextOptions { get; }

        public SqlTagsRepositoryTest()
        {
            this.ContextOptions = new DbContextOptionsBuilder<ApplicationContext>()
                .UseSqlite("Filename=TagsTest.db")
                .Options;
            Seed();
        }

        private void Seed()
        {
            using var context = new ApplicationContext(ContextOptions);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
                
            var entity = new Tag { Text = "first" };
            var entity1 = new Tag { Text = "second" };
            var entity2 = new Tag { Text = "third" };
            var entity3 = new Tag { Text = "firth" };

            var taskList1 = new TaskList { Name = "taskList1", Tags = new List<Tag> { entity, entity2 } };
            var taskList2 = new TaskList { Name = "taskList2", Tags = new List<Tag> { entity1, entity3 } };


            var task11 = new Task { Text = "task11", TaskList = taskList1, Tags = new List<Tag> { entity, entity3 } };
            var task12 = new Task { Text = "task12", TaskList = taskList1, Tags = new List<Tag> { entity1, entity2 } };
            var task21 = new Task { Text = "task21", TaskList = taskList2, Tags = new List<Tag> { entity } };
            var task22 = new Task { Text = "task22", TaskList = taskList2, Tags = new List<Tag> { entity1 } };

            context.AddRange(entity, entity1, entity2, entity3);
            context.AddRange(taskList1, taskList2);
            context.AddRange(task11, task12, task21, task22);
            context.SaveChanges();
        }

        [Fact]
        public void GetByTaskListTest()
        {
            using var context = new ApplicationContext(ContextOptions);
            var repository = new SqlTagsRepository(context);
            var taskList1 = new TaskList { Id = 1, Name = "taskList1" };
            var tags = repository.GetByTaskList(taskList1).ToList();
            Assert.NotEmpty(tags);
            Assert.Contains(tags, x => x.Text == "first");
            Assert.Contains(tags, x => x.Text == "third");
            var taskList2 = new TaskList { Id = 2, Name = "taskList2" };
            var tags1 = repository.GetByTaskList(taskList2).ToList();
            Assert.NotEmpty(tags1);
            Assert.Contains(tags1, x => x.Text == "second");
            Assert.Contains(tags1, x => x.Text == "firth");
        }

        [Fact]
        public void GetByTaskTest()
        {
            using var context = new ApplicationContext(ContextOptions);
            var repository = new SqlTagsRepository(context);
            var task = new Task { Id = 1};
            var tags = repository.GetByTask(task).ToList();
            Assert.NotEmpty(tags);
            Assert.Contains(tags, x => x.Text == "first");
            Assert.Contains(tags, x => x.Text == "firth");
            var task2 = new Task { Id = 2};
            var tags2 = repository.GetByTask(task2).ToList();
            Assert.NotEmpty(tags2);
            Assert.Contains(tags2, x => x.Text == "second");
            Assert.Contains(tags2, x => x.Text == "third");
            var task3 = new Task { Id = 3 };
            var tags3 = repository.GetByTask(task3).ToList();
            Assert.NotEmpty(tags3);
            Assert.Contains(tags3, x => x.Text == "first");
            var task4 = new Task { Id = 4 };
            var tags4 = repository.GetByTask(task4).ToList();
            Assert.NotEmpty(tags4);
            Assert.Contains(tags4, x => x.Text == "second");
        }
    }
}
