using Microsoft.EntityFrameworkCore;
using ToDoApi.Data;
using ToDoApi.Data.Models;
using ToDoApi.Data.Repositories;
using Xunit;

namespace ToDoApiTest.Repositories
{
    public class BaseSqlEntityRepositoryTest
    {
        protected DbContextOptions<ApplicationContext> ContextOptions { get; }

        public BaseSqlEntityRepositoryTest()
        {
            this.ContextOptions = new DbContextOptionsBuilder<ApplicationContext>()
                .UseSqlite("Filename=Test.db")
                .Options;
            Seed();
        }

        private void Seed()
        {
            using var context = new ApplicationContext(ContextOptions);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            var entity = new Tag { Text="first"};
            var entity1 = new Tag { Text = "second" };
            var entity2 = new Tag { Text = "third" };
            context.AddRange(entity, entity1, entity2);
            context.SaveChanges();
        }

        [Fact]
        public void GetByIdTest()
        {
            using var context = new ApplicationContext(ContextOptions);
            var repository = new BaseSqlEntityRepository<Tag, ApplicationContext>(context);
            var tag = repository.GetById(1);
            Assert.Equal("first", tag.Text);
            tag = repository.GetById(2);
            Assert.Equal("second", tag.Text);
            tag = repository.GetById(3);
            Assert.Equal("third", tag.Text);
            tag = repository.GetById(4);
            Assert.Null(tag);
        }

        [Fact]
        public void AddTest()
        {
            using var context = new ApplicationContext(ContextOptions);
            var repository = new BaseSqlEntityRepository<Tag, ApplicationContext>(context);
            var entity = new Tag { Text = "fourth" };
            var tag = repository.Add(entity);
            const int zeroId = 0;
            Assert.NotEqual(zeroId, tag.Id);
            entity = new Tag { Text = "fifth" };
            repository.Add(entity);
            Assert.NotEqual(zeroId, entity.Id);
        }

        [Fact]
        public void UpdateTest()
        {
            using var context = new ApplicationContext(ContextOptions);
            var repository = new BaseSqlEntityRepository<Tag, ApplicationContext>(context);
            var tag = repository.GetById(1);
            tag.Text = "first_mod";
            repository.Update(tag);
            var newTag = repository.GetById(1);
            Assert.Equal(tag.Text, newTag.Text);
        }

        [Fact]
        public void DeleteTest()
        {
            using var context = new ApplicationContext(ContextOptions);
            var repository = new BaseSqlEntityRepository<Tag, ApplicationContext>(context);
            repository.Delete(1);
            var tag = repository.GetById(1);
            Assert.Null(tag);
        }
    }
}
